#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <string>

#include "dns.h"

namespace nlib::dns {

  ip::IPv4Address resolve(bytes const & name) {
      addrinfo * result;
      // TODO: maybe use hints to allow only IPv4 address
      int const s = getaddrinfo(reinterpret_cast<char const *>(name.buf()), nullptr, nullptr, &result);
      if (s != 0) {
          // TODO: parse error
          throw std::invalid_argument("getaddrinfo failed: " + (std::string)gai_strerror(s));
      }

      for (addrinfo const * rp = result; rp != nullptr; rp = rp->ai_next) {
          if (rp->ai_family == AF_INET) {
              in_addr_t const inAddr = reinterpret_cast<sockaddr_in const *>(rp->ai_addr)->sin_addr.s_addr;
              freeaddrinfo(result);
              return ip::IPv4Address(inAddr);
          }
      }
      // TODO: parse error
      throw std::invalid_argument("no IPv4 address found");
  }

}
