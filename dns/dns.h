#ifndef NETWORKING_LIB_DNS_H
#define NETWORKING_LIB_DNS_H

#include "../lib.h"
#include "../ip/ip.h"

namespace nlib::dns {

  ip::IPv4Address resolve(bytes const &);

}
#endif //NETWORKING_LIB_DNS_H
