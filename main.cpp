#include <iostream>
#include <memory>

#include "http/http.h"

using namespace nlib::bytes_literals;

int main() {
    try {
        nlib::http::HTTPClientConnection clientConn = nlib::http::HTTPClientConnection::connect("www.google.com"_b);
        nlib::http::HTTPResponse response = clientConn.request(
            nlib::http::GET, "/"_b,
            nlib::http::HTTPHeaders({{"Host"_b, "www.google.com"_b},
                                     {"Accept-Encoding"_b, "gzip"_b}})
        );
        std::cout << "status: " << response.status() << "\n"
                     "headers:\n";
        for (auto const & header : response.headers().vec()) {
            std::cout << "    " << header.first << ": " << header.second << '\n';
        }
        std::cout << *response.body() << std::endl;

    } catch (std::exception const & e) {
        nlib::print_exception(e);
    }
}
