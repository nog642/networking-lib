#include <cstring>

#include <zlib.h>

#include "../lib.h"

namespace nlib {

  // Adapted from https://www.lemoda.net/c/zlib-open-read/
  // CHUNK is the size of the memory chunk used by the zlib routines.
  // windowBits is log2(max window size)
  template<unsigned int CHUNK = 0x4000, int windowBits = 15>
  bytes decode_gzip(bytes const & data) {
      bytes data_out;
      unsigned char in[CHUNK];
      unsigned char out[CHUNK];
      z_stream strm;
      strm.zalloc = Z_NULL;
      strm.zfree = Z_NULL;
      strm.opaque = Z_NULL;
      strm.next_in = in;
      strm.avail_in = 0;
      { // initialize strm
          // add 32 to windowBits to enable gzip decoding
          int zlib_status = inflateInit2(&strm, windowBits | 32);
          if (zlib_status < 0) {
              throw impossible("inflateInit2 returned a bad status of " + std::to_string(zlib_status));
          }
      }
      size_t pos = 0;
      while (true) {

          // copy up to sizeof(in) bytes from data to in
          size_t const bytes_read = std::min(sizeof(in), data.size() - pos);
          memcpy(in, &data[pos], bytes_read);
          pos += bytes_read;
          strm.avail_in = bytes_read;
          strm.next_in = in;

          do {
              strm.avail_out = CHUNK;
              strm.next_out = out;
              int zlib_status = inflate(&strm, Z_NO_FLUSH);
              switch (zlib_status) {
                  case Z_OK:
                  case Z_STREAM_END:
                  case Z_BUF_ERROR:
                      break;

                  default:
                      inflateEnd(&strm);
                      throw std::invalid_argument("Gzip error " + std::to_string(zlib_status));
              }
              data_out.extend(reinterpret_cast<std::byte const *>(out),
                              CHUNK - strm.avail_out);
          } while (strm.avail_out == 0);
          if (pos == data.size()) {
              inflateEnd(&strm);
              break;
          }
      }
      return data_out;
  }

}
