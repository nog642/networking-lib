#include "../lib.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/socket.h>

namespace nlib {

  ByteStream::ByteStream()
      : data(static_cast<size_t>(0))
      , cur(0)
  {}

  // Note: does not check if there are enough bytes available
  void ByteStream::read(std::byte * dest, size_t n) {
      memcpy(dest, &this->data[this->cur], n);
      this->cur += n;
  }

  void ByteStream::write(bytes const & newData) {
      this->data += newData;
  }

  size_t ByteStream::available() const {
      return this->data.size() - this->cur;
  }

  void ByteStream::get(std::byte & out) {
      out = this->data[this->cur];
      ++this->cur;
  }

  size_t ByteStream::tell() const {
      return this->cur;
  }

  void ByteStream::seek(size_t const pos) {
      this->cur = pos;
  }

  ssize_t PosixSocket::send(const nlib::bytes & data) {
      throw notimplemented("send not implemented for POSIX socket wrapper");
      // ssize_t const send_result = ::send(this->socket_fd, data.buf(), data.size(), 0);
      // if (send_result == -1) {
      //     throw std::system_error(errno, std::generic_category());
      // }
      // return send_result;
  }

  ssize_t PosixSocket::recv(size_t size, std::byte * buf) {
      ssize_t const recv_result = ::read(this->socket_fd, buf, size);
      if (recv_result == -1) {
          throw std::system_error(errno, std::generic_category());
      }
      return recv_result;
  }

  bytes PosixSocket::recv(size_t const size) {
      std::vector<std::byte> buf(size);
      ssize_t const recv_result = recv(size, buf.data());
      buf.resize(recv_result);
      return buf;
  }

  BufferedSocket::BufferedSocket(std::shared_ptr<Socket> const & socket)
      : buffer()
      , socket(socket)
  {}

  bytes BufferedSocket::read(size_t const n) {
      while (this->buffer.available() < n) {
          bytes const response = this->socket->recv(n - this->buffer.available());
          this->buffer.write(response);
      }
      std::vector<std::byte> result(n);
      this->buffer.read(&result[0], n);
      return result;
  }

  void BufferedSocket::write(bytes const & data) {
      size_t written = this->socket->send(data);
      while (written < data.size()) {
          // TODO: avoid this slice copy by using some sort of bytes view
          written += this->socket->send(data.slice(written, data.size()));
      }
  }

  bytes BufferedSocket::read_until(bytes const & target) {
      // TODO: instead of adding to total, which may require dynamic resizing, need to update
      //       the api for bytestream, and copy the whole output after finding the ending delimiter
      size_t matchIdx = 0;
      bytes total = ""_b;
      std::byte cur;
      while (true) {
          while (this->buffer.available()) {
              this->buffer.get(cur);
              size_t const curPos = this->buffer.tell();
              while (cur == target[matchIdx]) {
                  ++matchIdx;
                  if (matchIdx == target.size()) {
                      return total;
                  }
                  // TODO: handle buffer being empty here
                  this->buffer.get(cur);
              }
              total += cur;
              matchIdx = 0;
              this->buffer.seek(curPos);
          }
          bytes const response = this->socket->recv(2048);
          this->buffer.write(response);
      }
  }

  void print_exception(std::exception const & e, int level) {
      std::cerr << std::string(level, ' ') << "exception: " << e.what() << std::endl;
      try {
          std::rethrow_if_nested(e);
      } catch (const std::exception &e) {
          print_exception(e, level + 1);
      }
  }

  exception::exception(std::string const & what_arg) {
      this->what_arg = what_arg;
  }
  char const * exception::what() const noexcept {
      return this->what_arg.c_str();
  }

}
