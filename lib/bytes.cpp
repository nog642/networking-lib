#include <cstring>

#include <string>

#include "../lib.h"

namespace nlib {

  bytes::bytes(std::vector<std::byte> && v)
      : vector(std::move(v))
  {}

  bytes::bytes(std::string const & s)
      : vector(reinterpret_cast<std::byte const *>(s.data()),
               reinterpret_cast<std::byte const *>(s.data()) + s.size())
  {}

  bytes::bytes(std::byte const * const start, size_t const len)
      : vector(start, start + len)
  {}

  bytes::bytes(size_t const size)
      : vector(size)
  {}

  namespace bytes_literals {
    std::byte operator "" _b(char const c) {
        return std::byte(c);
    }
    bytes operator "" _b(char const * const s, size_t const size) {
        return bytes(reinterpret_cast<std::byte const *>(s), size);
    }
  }

  std::ostream & operator<<(std::ostream & o, bytes const & b) {
      o.write(reinterpret_cast<char const *>(b.buf()), b.size());
      return o;
  }

  bytes & bytes::operator+=(bytes const & other) {
      this->vector.insert(this->vector.end(), other.vector.begin(), other.vector.end());
      return *this;
  }

  bytes & bytes::operator+=(std::byte b) {
      this->vector.push_back(b);
      return *this;
  }

  bytes bytes::operator+(bytes const & b2) const {
      bytes sum = *this;  // copy this to sum
      sum += b2;
      return sum;
  }

  std::byte & bytes::operator[](size_t const offset) {
      return this->vector[offset];
  }

  std::byte const & bytes::operator[](size_t const offset) const {
      return this->vector[offset];
  }

  bool bytes::operator==(bytes const & other) const {
      if (this->size() != other.size()) {
          return false;
      }
      for (size_t i = 0; i < this->size(); ++i) {
          if ((*this)[i] != other[i]) {
              return false;
          }
      }
      return true;
  }

  bytes::iterator bytes::begin() {
      return this->vector.begin();
  }
  bytes::const_iterator bytes::begin() const {
      return this->vector.begin();
  }
  bytes::iterator bytes::end() {
      return this->vector.end();
  }
  bytes::const_iterator bytes::end() const {
      return this->vector.end();
  }

  size_t bytes::size() const {
      return this->vector.size();
  }

  std::byte const * bytes::buf() const {
      return this->vector.data();
  }

  std::byte * bytes::buf() {
      return this->vector.data();
  }

  void bytes::resize(size_t const n) {
      this->vector.resize(n);
  }

  void bytes::extendSize(size_t const n) {
      this->vector.resize(this->vector.size() + n);
  }

  void bytes::extend(std::byte const * data, size_t n) {
      // TODO: the reserve may not be necessary
      this->vector.reserve(this->vector.size() + n);
      this->vector.insert(this->vector.end(), data, data + n);
  }

  bytes bytes::slice(size_t start, size_t stop) const {
      // TODO: maybe raise exception if start/stop are out of bounds
      return std::vector<std::byte>(this->vector.begin() + start,
                                    this->vector.begin() + stop);
  }

  ssize_t bytes::find(bytes const & target) const {
      return this->findAfter(0, target);
  }

  ssize_t bytes::findAfter(size_t const start, bytes const & target) const {
      // adapted from http://src.gnu-darwin.org/src/lib/libc/string/strnstr.c.html
      if (target.size() == 0) {
          return 0;
      }
      std::byte const * s = &(*this)[start];
      std::byte const * const find = &target[1];
      size_t slen = this->size();
      std::byte const c = target[0];
      std::byte sc;
      size_t const len = target.size() - 1;
      do {
          do {
              if (slen < 1) {
                  return -1;
              }
              sc = *s;
              ++s;
              --slen;
          } while (sc != c);
          if (len > slen) {
              return -1;
          }
      } while (std::strncmp(reinterpret_cast<char const *>(s),
                            reinterpret_cast<char const *>(find),
                            len) != 0);
      return s - this->buf() - 1;
  }

  // unsafe overflow
  uint_fast16_t bytes::parseU16f_UO() const {
      uint_fast16_t n = 0;
      for (std::byte b : *this) {
          auto const val = static_cast<unsigned char>(b) - '0';
          if (val < 0 || val > 9) {
              throw std::invalid_argument("Non-digit found");
          }
          n = n * 10 + val;
      }
      return n;
  }

  std::string bytes::repr() const {
      std::string s{"\""};
      for (std::byte const b : *this) {
          char const c = static_cast<char>(b);
          switch (c) {
              case '\\':
                  s += "\\\\";
                  break;
              case '\t':
                  s += "\\t";
                  break;
              case '\n':
                  s += "\\n\\\n";
                  break;
              case '\r':
                  s += "\\r";
                  break;
              default:
                  s += c;
          }
      }
      s += '"';
      return s;
  }

  std::string bytes::toString() const {
      return std::string(reinterpret_cast<char const *>(this->buf()),
                         reinterpret_cast<char const *>(this->buf() + this->size()));
  }

  std::string bytes::hex() const {
      std::string s;
      size_t const countIn = this->size() * 2;
      s.resize_and_overwrite(countIn, [this, countIn](char * p, std::string::size_type count) {
          static constexpr char hexmap[16]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                           'a', 'b', 'c', 'd', 'e', 'f'};
          int i = 0;
          int si = 0;
          while (true) {
              int const byte = static_cast<int>((*this)[i]);
              p[si] = hexmap[(byte & 0xf0) >> 4];
              p[si + 1] = hexmap[byte & 0xf];
              ++i;
              if (i >= this->size()) {
                  // We should be able to return count, but GCC has a bug that
                  // makes count higher than expected sometimes:
                  // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=104222
                  return countIn;
              }
              si += 2;
          }
      });
      return s;
  }

  unsigned long long bytes::parseUInt() const {
      // TODO: check for overflow
      unsigned long long n = 0;
      for (std::byte const b : *this) {
          auto const val = static_cast<unsigned char>(b) - '0';
          if (val < 0 || val > 9) {
              throw std::invalid_argument("Non-digit found");
          }
          n = n * 10 + val;
      }
      return n;
  }

  unsigned long long bytes::parseUIntHex() const {
      // TODO: check for overflow
      unsigned long long n = 0;
      for (std::byte const b : *this) {
          unsigned char const c = static_cast<unsigned char>(b);
          uint_fast8_t val;
          if (c >= '0') {
              if (c >= 'a') {
                  if (c >= 'A' && c <= 'F') {
                      val = c - 'A' + 10;
                  } else if (c <= 'f') {
                      val = c - 'a' + 10;
                  } else {
                      throw std::invalid_argument("Non-hex-digit found");
                  }
              } else if (c <= '9') {
                  val = c - '0';
              } else {
                  throw std::invalid_argument("Non-hex-digit found");
              }
          } else {
              throw std::invalid_argument("Non-hex-digit found");
          }
          n = n * 16 + val;
      }
      return n;
  }

}
