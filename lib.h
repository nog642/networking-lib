#ifndef NETWORKING_LIB_LIB_H
#define NETWORKING_LIB_LIB_H
#include <cstddef>
#include <cstring>
#include <iostream>
#include <functional>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string_view>
#include <vector>
#include <sys/types.h>

namespace nlib {

  // As of May 2024, GCC does not implement std::start_lifetime_as, even for C++23, so we have to provide it ourselves
  // Adapted from https://godbolt.org/z/VuRtNC and https://gist.github.com/GavinRay97/b69cb6ebab6a0e13d2cfe74a6ed7cdc6
  template<typename T>
  T * start_lifetime_as(void * ptr) noexcept {
      // std::memmove will implicitly create objects within the
      // destination prior to copying, which means that we
      // can force an unsigned char to be created that has
      // the same value as whatever the storage had prior.
      // since the destination is the same as the source,
      // we can deduce from the intent in the paper that
      // because the object is created prior to when the copy
      // occurs some kind of pointer rebinding must occur
      // in order to copy into the created object.
      // one of the preconditions of std::launder is that an object of type
      // T exists within the storage, so this provides us with the UB we need
      // to force the creation of the object.
      return std::launder(static_cast<T *>(std::memmove(ptr, ptr, sizeof(T))));
  }
  template<typename T>
  T const * start_lifetime_as(void const * ptr) noexcept {
      return start_lifetime_as<T>(const_cast<void *>(ptr));
  }

  class exception : public std::exception {
    private:
      std::string what_arg;
    public:
      explicit exception(std::string const & what_arg);
      char const * what() const noexcept;
  };

  class notimplemented : public exception {
    public:
      explicit notimplemented(std::string const & what_arg) : exception(what_arg) {}
  };

  class bytes {
    private:
      std::vector<std::byte> vector;
    public:
      bytes() = default;
      bytes(std::vector<std::byte> &&);
      bytes(std::string const &);
      explicit bytes(std::byte const *, size_t);
      explicit bytes(size_t);

      friend std::ostream & operator<<(std::ostream &, bytes const &);
      bytes operator+(bytes const &) const;
      bytes & operator+=(bytes const &);
      bytes & operator+=(std::byte);
      std::byte & operator[](size_t);
      std::byte const & operator[](size_t) const;
      bool operator==(bytes const &) const;
      bool operator!=(bytes const & other) const {return !(*this == other);}

      typedef std::vector<std::byte>::iterator iterator;
      typedef std::vector<std::byte>::const_iterator const_iterator;
      iterator begin();
      const_iterator begin() const;
      iterator end();
      const_iterator end() const;

      size_t size() const;
      std::byte const * buf() const;
      std::byte * buf();

      void resize(size_t);
      void extendSize(size_t);
      void extend(std::byte const *, size_t);
      bytes slice(size_t, size_t) const;
      ssize_t find(bytes const &) const;
      ssize_t findAfter(size_t, bytes const &) const;
      template<typename func>
      ssize_t findByte(func comp) const {
          for (size_t i = 0; i < this->size(); ++i) {
              if (comp((*this)[i])) {
                  return i;
              }
          }
          return -1;
      }
      std::string repr() const;
      std::string toString() const;
      std::string hex() const;
      uint_fast16_t parseU16f_UO() const;
      unsigned long long parseUInt() const;
      unsigned long long parseUIntHex() const;

      // uncomment this to debug copy/move semantics
//      bytes & operator=(bytes const &) = default;
//      bytes & operator=(bytes &&) noexcept = default;
//      bytes(bytes const & b) : vector(b.vector) {
//          std::cout << "COPY constructor called on \"" << *this << '"' << std::endl;
//      }
//      bytes(bytes && b) noexcept : vector(std::move(b.vector)) {
//          std::cout << "move constructor called on \"" << *this << '"' << std::endl;
//      }
  };

  namespace bytes_literals {
    std::byte operator "" _b(char);
    bytes operator "" _b(char const *, size_t);
  }
  using namespace bytes_literals;

  class ByteStream {
    private:
      bytes data;
      size_t cur;
    public:
      ByteStream();
      void read(std::byte *, size_t);
      void write(bytes const &);
      size_t available() const;
      void get(std::byte &);
      size_t tell() const;
      void seek(size_t);
  };

  // TODO: maybe replace this with a concept once all shared_ptr usages are replaced with templates
  class Socket {
    public:
      virtual ssize_t send(bytes const &) = 0;
      virtual bytes recv(size_t) = 0;
      virtual ssize_t recv(size_t, std::byte *) = 0;
  };

  class PosixSocket : public Socket {
    private:
      int socket_fd;
    public:
      explicit PosixSocket(int const socket_fd) : socket_fd(socket_fd) {}
      ssize_t send(bytes const &) override;
      ssize_t recv(size_t, std::byte *) override;  // TODO: maybe make this return size_t
      bytes recv(size_t) override;
  };

  template<typename WrappedSocket, size_t bufSize> requires std::derived_from<WrappedSocket, Socket>
  class PerPacketSocket : public Socket {
    private:
      WrappedSocket socket;
      std::byte buffer[bufSize];
      size_t cur;
      size_t end;

      size_t remaining() const { return end - cur; }
    public:
      explicit PerPacketSocket(WrappedSocket const & socket) : socket(socket) {}
      ssize_t send(bytes const &) override {
          throw notimplemented("send not implemented for per-packet socket wrapper");
      }
      bytes recv(size_t n) override {
          // TODO: preallocate bytes
          bytes data;
          while (true) {
              if (cur < end) {
                  if (n <= remaining()) {
                      data.extend(&buffer[cur], n);
                      cur += n;
                      return data;
                  }
                  // WARNING: read is crossing packet boundary
                  data.extend(&buffer[cur], remaining());
                  n -= remaining();
              }
              cur = 0;
              end = socket.recv(bufSize, buffer);
          }
      }
      ssize_t recv(size_t n, std::byte * const buf) override {
          int i = 0;
          while (true) {
              if (cur < end) {
                  if (n <= remaining()) {
                      std::memcpy(&buf[i], &buffer[cur], n);
                      cur += n;
                      return n;
                  }
                  // WARNING: read is crossing packet boundary
                  std::memcpy(&buf[i], &buffer[cur], remaining());
                  i += remaining();
                  n -= remaining();
              }
              cur = 0;
              end = socket.recv(bufSize, buffer);
          }
      }
  };

  class BufferedSocket {
    private:
      ByteStream buffer;
      std::shared_ptr<Socket> socket;
    public:
      explicit BufferedSocket(std::shared_ptr<Socket> const &);
      bytes read(size_t);
      void write(bytes const &);
      bytes read_until(bytes const &);
  };

  void print_exception(std::exception const &, int=1);

  class impossible : public std::logic_error {
    public:
      explicit impossible(std::string const & what_arg) : std::logic_error(what_arg) {}
  };

}

#include "lib/zlib_wrapper.h"

namespace std {

  template<> struct hash<nlib::bytes> {
      size_t operator()(nlib::bytes const & b) const noexcept {
          return hash<string_view>()(string_view(reinterpret_cast<char const *>(b.buf()), b.size()));
      }
  };

}

#endif //NETWORKING_LIB_LIB_H
