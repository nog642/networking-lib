#ifndef NETWORKING_LIB_IP_H
#define NETWORKING_LIB_IP_H
#include <memory>
#include <string>
#include <arpa/inet.h>
#include "../lib.h"

namespace nlib::ip {

  class IPv4Address {
    private:
      in_addr_t network_order_addr;
    public:
      explicit IPv4Address(in_addr_t);
      static IPv4Address from_string(std::string const & ip_addr);
      in_addr_t get_in_addr_t() const { return network_order_addr; }
      bool operator==(IPv4Address const &) const;
      friend std::ostream & operator<<(std::ostream &, IPv4Address const &);
  };

  class IPv6Address {
    private:
      struct in6_addr network_order_addr;
    public:
      explicit IPv6Address(struct in6_addr const &);
      // static IPv6Address from_string(std::string const & ip_addr);
      struct in6_addr const & get_in6_addr() const { return network_order_addr; }
      // bool operator==(IPv4Address const &) const;
      friend std::ostream & operator<<(std::ostream &, IPv6Address const &);
  };

  class IPv6Packet {
    private:
      uint8_t trafficClass;
      uint32_t flowLabel : 20;
      uint16_t payloadLength;
      uint8_t nextHeader;
      uint8_t hopLimit;
    public:
      IPv6Address const sourceAddr;
      IPv6Address const destAddr;
      bytes const payload;  // TODO: make this a view

      IPv6Packet(uint8_t, uint32_t, uint16_t, uint8_t, uint8_t, IPv6Address, IPv6Address, std::byte const *);
      [[nodiscard]] uint16_t payloadSize() const { return payloadLength; }
      // // takes ownership of the body, will delete it upon destruction
      // HTTPResponse(HTTPHeaders &&, bytes const *, enum HTTPResponseStatus);
      // ~HTTPResponse();
      // enum HTTPResponseStatus status() const;
      // HTTPHeaders const & headers() const;
      // bool hasBody() const;
      // bytes const * rawBody() const;
      // bytes const * body() const;
  };

  class IPv6Socket {
      PerPacketSocket<PosixSocket, 65535> socket;  // TODO: generalize
    public:
      explicit IPv6Socket(int sock_fd) : socket(PosixSocket(sock_fd)) {}
      IPv6Packet readPacket();  // TODO: make this not copy data
  };

}
#endif //NETWORKING_LIB_IP_H
