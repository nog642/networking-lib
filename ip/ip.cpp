#include "ip.h"
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <system_error>


namespace nlib::ip {

  IPv4Address::IPv4Address(in_addr_t network_order_addr)
      : network_order_addr(network_order_addr)
  {}

  IPv4Address IPv4Address::from_string(std::string const & ip_addr) {
      std::uint32_t network_order_addr;

      int const inet_pton_status = inet_pton(AF_INET, ip_addr.c_str(), &network_order_addr);
      if (inet_pton_status == 0) {
          throw std::invalid_argument("Invalid IP address: " + ip_addr);
      } else if (inet_pton_status == -1) {
          if (errno == EAFNOSUPPORT) {
              // inet_pton af should is hardcoded so this should be impossible
              throw nlib::impossible("inet_pton was passed incorrect af");
          } else {
              // should not happen according to the docs but whatever
              throw std::system_error(errno, std::generic_category());
          }
      } // else inet_pton_status == 1

      return IPv4Address(network_order_addr);
  }

  bool IPv4Address::operator==(IPv4Address const & other) const {
      return this->network_order_addr == other.network_order_addr;
  }

  std::ostream & operator<<(std::ostream & o, IPv4Address const & ip) {
      socklen_t const size = 16;  // max string size of IPv4
      char dst[size];
      in_addr ip_struct{ip.get_in_addr_t()};
      char const * const s = inet_ntop(AF_INET, &ip_struct, dst, size);
      o.write(s, static_cast<std::streamsize>(strlen(s)));
      return o;
  }

  IPv6Address::IPv6Address(struct in6_addr const & network_order_addr)
      : network_order_addr(network_order_addr)
  {}

  std::ostream & operator<<(std::ostream & o, IPv6Address const & ip) {
      socklen_t const size = 40;  // max string size of IPv6
      char dst[size];
      char const * const s = inet_ntop(AF_INET6, &ip.get_in6_addr(), dst, size);
      o.write(s, static_cast<std::streamsize>(strlen(s)));
      return o;
  }

  IPv6Packet::IPv6Packet(uint8_t const trafficClass, uint32_t const flowLabel, uint16_t const payloadLength,
                         uint8_t const nextHeader, uint8_t const hopLimit, IPv6Address const sourceAddr,
                         IPv6Address const destAddr, std::byte const * const payloadStart)
      : trafficClass(trafficClass)
      , flowLabel(flowLabel)
      , payloadLength(payloadLength)
      , nextHeader(nextHeader)
      , hopLimit(hopLimit)
      , sourceAddr(sourceAddr)
      , destAddr(destAddr)
      , payload(payloadStart, payloadLength)
  {}

  IPv6Packet IPv6Socket::readPacket() {
      // TODO: handle jumbo payload
      bytes const header = socket.recv(40);
      uint16_t const payloadLength = ntohs(*nlib::start_lifetime_as<uint16_t>(&header[4]));
      bytes const payload = socket.recv(payloadLength);
      return {
          static_cast<uint8_t>(((header[0] & static_cast<std::byte>(0xf)) << 4)
              | ((header[1] & static_cast<std::byte>(0xf0)) >> 4)),
          ((static_cast<unsigned int>(header[1]) & 0xfu) << 16)
              | ntohs(*nlib::start_lifetime_as<uint16_t>(&header[2])),
          payloadLength,
          static_cast<uint8_t const>(header[6]),
          static_cast<uint8_t const>(header[7]),
          IPv6Address(*nlib::start_lifetime_as<in6_addr>(&header[8])),
          IPv6Address(*nlib::start_lifetime_as<in6_addr>(&header[24])),
          &payload[0]
      };
  }

}
