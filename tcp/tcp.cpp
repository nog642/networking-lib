#include "tcp.h"
#include <arpa/inet.h>
#include <cerrno>
#include <iostream>

namespace nlib::tcp {

  TCPSocket TCPSocket::connect(nlib::ip::IPv4Address const & ip_addr, int const port) {
      int const socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
      if (socket_fd == -1) {
          throw std::system_error(errno, std::generic_category());
      }

      sockaddr_in const socket_addr = {
          .sin_family=AF_INET,
          .sin_port=htons(port),
          .sin_addr={.s_addr=ip_addr.get_in_addr_t()}
      };

      int const connect_status = ::connect(
          socket_fd,
          reinterpret_cast<sockaddr const *>(&socket_addr),
          sizeof(socket_addr)
      );
      if (connect_status == -1) {
          throw std::system_error(errno, std::generic_category());
      }  // else connect_status == 0

      return TCPSocket(socket_fd);
  }

  ssize_t TCPSocket::send(bytes const & data) {
      ssize_t const send_result = ::send(this->socket_fd, data.buf(), data.size(), 0);
      if (send_result == -1) {
          throw std::system_error(errno, std::generic_category());
      }
      return send_result;
  }

  bytes TCPSocket::recv(size_t const size) {
      std::vector<std::byte> buf(size);
      ssize_t const recv_result = recv(size, buf.data());
      buf.resize(recv_result);
      return buf;
  }

  ssize_t TCPSocket::recv(size_t const size, std::byte * const buf) {
      ssize_t const recv_result = ::recv(this->socket_fd, buf, size, 0);
      if (recv_result == -1) {
          throw std::system_error(errno, std::generic_category());  // TODO: not system category?
      }
      return recv_result;
  }

}
