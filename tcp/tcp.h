#ifndef NETWORKING_LIB_TCP_H
#define NETWORKING_LIB_TCP_H
#include <cstddef>
#include "../lib.h"
#include "../ip/ip.h"

namespace nlib::tcp {

  // TODO: wrap Socket (PosixSocket) instead of file descriptor exactly
  class TCPSocket : public nlib::Socket {
    private:
      int socket_fd;
      explicit TCPSocket(int const socket_fd) : socket_fd(socket_fd) {}
    public:
      static TCPSocket connect(ip::IPv4Address const &, int);
      ssize_t send(bytes const &) override;
      bytes recv(size_t) override;
      ssize_t recv(size_t, std::byte *) override;
  };

}
#endif //NETWORKING_LIB_TCP_H
