#ifndef NETWORKING_LIB_HTTP_H
#define NETWORKING_LIB_HTTP_H

#include <memory>
#include <vector>
#include <unordered_map>

#include "../tcp/tcp.h"

namespace nlib::http {

  enum HTTTPRequestMethod {
      GET,
      HEAD,
      POST,
      PUT,
      DELETE,
      CONNECT,
      OPTIONS,
      TRACE,
      PATCH
  };

#include "statusCodes/HTTPResponseStatus.h"

  class HTTPHeaders {
    private:
      typedef std::vector<std::pair<bytes, bytes>> vector_type;
      typedef std::vector<vector_type::size_type> idxs_type;
      typedef std::unordered_map<bytes, idxs_type> idx_map_type;
      typedef std::unordered_map<bytes, bytes> cache_type;
      vector_type headers;
      idx_map_type map;
      mutable cache_type cache;  // only used for comma-joined duplicate headers
      static bytes processHeaderName(bytes const &);
      static bytes processHeaderValue(bytes const &);
    public:
      HTTPHeaders(vector_type const &);
      HTTPHeaders(std::initializer_list<std::array<bytes, 2>>);

      void add(bytes &&, bytes &&);
      bytes getBytes() const;
      vector_type const & vec() const;

      // fast versions require all lowercase argument
      // normal versions are case-insensitive
      bytes const & get(bytes const &) const;
      bytes const & getFast(bytes const &) const;
      bytes const * find(bytes const &) const;
      bytes const * findFast(bytes const &) const;
      bool contains(bytes const &) const;
      bool containsFast(bytes const &) const;
  };

//  class HTTPRequest {
//    public:
//      enum HTTTPRequestMethod method;
//      HTTPHeaders headers;
//  };

  class HTTPResponse {
    private:
      enum HTTPResponseStatus statusCode;
      HTTPHeaders headersObj;
      bytes const * bodyBytes;
      mutable bytes const * decodedBodyBytes;  // cache
    public:
      // takes ownership of the body, will delete it upon destruction
      HTTPResponse(HTTPHeaders &&, bytes const *, enum HTTPResponseStatus);
      ~HTTPResponse();
      enum HTTPResponseStatus status() const;
      HTTPHeaders const & headers() const;
      bool hasBody() const;
      bytes const * rawBody() const;
      bytes const * body() const;
  };

  class HTTPClientConnection {
    private:
      BufferedSocket socket;
      void parseHeaders(HTTPHeaders &, bool);
    public:
      explicit HTTPClientConnection(std::shared_ptr<Socket> const &);

      HTTPResponse request(enum HTTTPRequestMethod, bytes const &, HTTPHeaders const &);

      static HTTPClientConnection connect(bytes const &);
  };

  class httpexception : public exception {
    public:
      explicit httpexception(std::string const & what_arg) : exception(what_arg) {}
  };

}
#endif //NETWORKING_LIB_HTTP_H
