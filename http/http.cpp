#include <cctype>

#include <algorithm>

#include "http.h"
#include "../dns/dns.h"

namespace nlib::http {

#include "statusCodes/HTTPResponseStatus.cpp"

  namespace {  // private to this file
    bytes getHTTTPRequestMethodBytes(enum HTTTPRequestMethod method) {
        switch (method) {
            case GET:
                return "GET"_b;
            case HEAD:
                return "HEAD"_b;
            case POST:
                return "POST"_b;
            case PUT:
                return "PUT"_b;
            case DELETE:
                return "DELETE"_b;
            case CONNECT:
                return "CONNECT"_b;
            case OPTIONS:
                return "OPTIONS"_b;
            case TRACE:
                return "TRACE"_b;
            case PATCH:
                return "PATCH"_b;
        }
        throw impossible("getHTTTPRequestMethodBytes did not match any case");
    }
    bool responseHasBody(enum HTTTPRequestMethod requestMethod, enum HTTPResponseStatus status) {
        return (
            (requestMethod != HEAD) &&
            (status >= 200) &&
            (status != 204) &&
            (status != 304)
        );
    }
    static bytes DISALLOWED_TRAILERS[] = {
        // TODO: maybe make this a set

        // message framing
        "Transfer-Encoding"_b, "Content-Length"_b,

        // routing
        "Host"_b,

        // request modifiers
        "Cache-Control"_b, "Expect"_b, "Max-Forwards"_b, "Pragma"_b, "Range"_b, "TE"_b,
        "If-Match"_b, "If-None-Match"_b, "If-Modified-Since"_b, "If-Unmodified-Since"_b, "If-Range"_b,

        // authentication
        "WWW-Authenticate"_b, "Authorization"_b, "Proxy-Authenticate"_b, "Proxy-Authorization"_b, "Set-Cookie"_b,

        // response control data
        "Age"_b, "Expires"_b, "Date"_b, "Location"_b, "Retry-After"_b, "Vary"_b, "Warning"_b,

        // determining how to process the payload
        "Content-Encoding"_b, "Content-Type"_b, "Content-Range"_b, "Trailer"_b
    };
  }

  HTTPClientConnection::HTTPClientConnection(std::shared_ptr<Socket> const & socket)
      : socket(socket)
  {}

  HTTPHeaders::HTTPHeaders(std::vector<std::pair<bytes, bytes>> const & headers) {
      for (size_t i = 0; i < headers.size(); ++i) {
          this->map.emplace(HTTPHeaders::processHeaderName(headers[i].first), i);
      }
      this->headers = headers;
  }

  HTTPHeaders::HTTPHeaders(std::initializer_list<std::array<bytes, 2>> const l)
      : headers()
  {
      this->headers.reserve(l.size());
      size_t i = 0;
      std::transform(l.begin(), l.end(), std::back_inserter(this->headers),
                     [&](std::array<bytes, 2> const & header) -> std::pair<bytes, bytes> {
          this->map.emplace(HTTPHeaders::processHeaderName(header[0]), idxs_type{i});
          ++i;
          return {header[0], header[1]};
      });
  }

  bytes HTTPHeaders::processHeaderName(bytes const & name) {
      bytes newName(name.size());
      transform(name.begin(), name.end(), newName.begin(),
                [](std::byte const c) -> std::byte {
          unsigned char const val = static_cast<unsigned char const>(
              std::tolower(static_cast<unsigned char const>(c))
          );
          // TODO: check that val is valid for an HTTP header
          return std::byte(val);
      });
      return newName;
  }

  bytes HTTPHeaders::processHeaderValue(bytes const &) {
      // TODO
  }

  void HTTPHeaders::add(bytes && name, bytes && val) {
      // TODO: make sure this moves. maybe use a bytes view so name is not duplicated
      bytes processedName = HTTPHeaders::processHeaderName(name);
      idx_map_type::const_iterator const loc = this->map.find(processedName);
      if (loc == this->map.end()) {
          this->map.emplace(std::move(processedName),
                            idxs_type{this->headers.size()});
      } else {
          this->map.at(processedName).push_back(this->headers.size());
      }
      this->headers.emplace_back(std::move(name), std::move(val));
  }

  bytes HTTPHeaders::getBytes() const {
      bytes b;
      for (std::pair<bytes, bytes> const & header : this->headers) {
          b += header.first;
          b += ":"_b;
          b += header.second;
          b += "\r\n"_b;
      }
      return b;
  }

  HTTPHeaders::vector_type const & HTTPHeaders::vec() const {
      return this->headers;
  }

  bytes const & HTTPHeaders::get(bytes const & name) const {
      bytes processedName = HTTPHeaders::processHeaderName(name);
      idxs_type const headerIdxs = this->map.at(processedName);
      if (headerIdxs.size() == 1) {
          // there is only one header with this name
          // return a reference to the value
          return this->headers[headerIdxs[0]].second;
      } else {
          // there are multiple headers with this name
          // they need to be concatenated with comma delimiters
          // check if this value is cached
          cache_type::const_iterator const cacheLoc = this->cache.find(processedName);
          if (cacheLoc == this->cache.end()) {
              // return a reference to the cached value
              return cacheLoc->second;
          } else {
              // the value is not cached, so compute it.
              bytes value = this->headers[headerIdxs[0]].second;
              for (std::vector<vector_type::size_type>::size_type i = 1; i < headerIdxs.size(); ++i) {
                  value += ','_b;
                  value += this->headers[headerIdxs[i]].second;
              }
              // save the value in cache and return a reference to it
              return this->cache.emplace(std::move(processedName), value).first->second;
          }
      }
  }
  bytes const & HTTPHeaders::getFast(bytes const & name) const {
      idxs_type const headerIdxs = this->map.at(name);
      if (headerIdxs.size() == 1) {
          // there is only one header with this name
          // return a reference to the value
          return this->headers[headerIdxs[0]].second;
      } else {
          // there are multiple headers with this name
          // they need to be concatenated with comma delimiters
          // check if this value is cached
          cache_type::const_iterator const cacheLoc = this->cache.find(name);
          if (cacheLoc == this->cache.end()) {
              // return a reference to the cached value
              return cacheLoc->second;
          } else {
              // the value is not cached, so compute it.
              bytes value = this->headers[headerIdxs[0]].second;
              for (std::vector<vector_type::size_type>::size_type i = 1; i < headerIdxs.size(); ++i) {
                  value += ','_b;
                  value += this->headers[headerIdxs[i]].second;
              }
              // save the value in cache and return a reference to it
              return this->cache.emplace(name, value).first->second;
          }
      }
  }
  bytes const * HTTPHeaders::find(bytes const & name) const {
      bytes processedName = HTTPHeaders::processHeaderName(name);
      idx_map_type::const_iterator const loc = this->map.find(processedName);
      if (loc == this->map.end()) {
          // header is missing
          return nullptr;
      }
      idxs_type const headerIdxs = loc->second;
      if (headerIdxs.size() == 1) {
          // there is only one header with this name
          // return a pointer to the value
          return &this->headers[headerIdxs[0]].second;
      } else {
          // there are multiple headers with this name
          // they need to be concatenated with comma delimiters
          // check if this value is cached
          cache_type::const_iterator const cacheLoc = this->cache.find(processedName);
          if (cacheLoc == this->cache.end()) {
              // return a pointer to the cached value
              return &cacheLoc->second;
          } else {
              // the value is not cached, so compute it.
              bytes value = this->headers[headerIdxs[0]].second;
              for (std::vector<vector_type::size_type>::size_type i = 1; i < headerIdxs.size(); ++i) {
                  value += ','_b;
                  value += this->headers[headerIdxs[i]].second;
              }
              // save the value in cache and return a pointer to it
              return &this->cache.emplace(std::move(processedName), value).first->second;
          }
      }
  }
  bytes const * HTTPHeaders::findFast(bytes const & name) const {
      idx_map_type::const_iterator const loc = this->map.find(name);
      if (loc == this->map.end()) {
          // header is missing
          return nullptr;
      }
      idxs_type const headerIdxs = loc->second;
      if (headerIdxs.size() == 1) {
          // there is only one header with this name
          // return a pointer to the value
          return &this->headers[headerIdxs[0]].second;
      } else {
          // there are multiple headers with this name
          // they need to be concatenated with comma delimiters
          // check if this value is cached
          cache_type::const_iterator const cacheLoc = this->cache.find(name);
          if (cacheLoc == this->cache.end()) {
              // return a pointer to the cached value
              return &cacheLoc->second;
          } else {
              // the value is not cached, so compute it.
              bytes value = this->headers[headerIdxs[0]].second;
              for (std::vector<vector_type::size_type>::size_type i = 1; i < headerIdxs.size(); ++i) {
                  value += ','_b;
                  value += this->headers[headerIdxs[i]].second;
              }
              // save the value in cache and return a pointer to it
              return &this->cache.emplace(name, value).first->second;
          }
      }
  }
  bool HTTPHeaders::contains(bytes const & name) const {
      return this->map.count(HTTPHeaders::processHeaderName(name));
  }
  bool HTTPHeaders::containsFast(bytes const & name) const {
      return this->map.count(name);
  }

  HTTPResponse::HTTPResponse(HTTPHeaders && headers, bytes const * body, enum HTTPResponseStatus status)
      : statusCode(status)
      , headersObj(std::move(headers))
      , bodyBytes(body)
      , decodedBodyBytes(nullptr)
  {}
  HTTPResponse::~HTTPResponse() {
      delete this->bodyBytes;
      if (this->decodedBodyBytes != nullptr && this->decodedBodyBytes != this->bodyBytes) {
          delete this->decodedBodyBytes;
      }
  }
  enum HTTPResponseStatus HTTPResponse::status() const {
      return this->statusCode;
  }
  HTTPHeaders const & HTTPResponse::headers() const {
      return this->headersObj;
  }
  bool HTTPResponse::hasBody() const {
      return this->bodyBytes != nullptr;
  }
  bytes const * HTTPResponse::rawBody() const {
      return this->bodyBytes;
  }
  bytes const * HTTPResponse::body() const {
      if (this->decodedBodyBytes != nullptr) {
          return this->decodedBodyBytes;
      }
      if (this->bodyBytes == nullptr) {
          return nullptr;
      }
      bytes const * contentEncoding = this->headersObj.findFast("content-encoding"_b);
      if (contentEncoding == nullptr) {
          // no encoding specified
          this->decodedBodyBytes = this->bodyBytes;

      } else if (*contentEncoding == "gzip"_b) {
          this->decodedBodyBytes = new bytes(decode_gzip(*this->bodyBytes));

      } else {
          throw notimplemented("Content encoding not implemented: " + contentEncoding->toString());
      }
      return this->decodedBodyBytes;
  }

  void HTTPClientConnection::parseHeaders(HTTPHeaders & headers, bool trailer) {
      while (true) {
          bytes header = this->socket.read_until("\r\n"_b);
          if (header.size() == 0) {
              break;
          }
          size_t const colon = header.find(":"_b);
          bytes value = header.slice(colon + 1, header.size());

          // trim leading whitespace
          value = value.slice(value.findByte([](std::byte b) noexcept -> bool {
              return b != std::byte{' '} && b != std::byte{'\t'};
          }), value.size());
          // TODO: trim trailing whitespace

          bytes name = header.slice(0, colon);

          if (trailer) {
              // we are processing chunked trailers
              bool disallowed = false;
              for (bytes const & disallowedTrailer : DISALLOWED_TRAILERS) {
                  if (name == disallowedTrailer) {
                      disallowed = true;
                      break;
                  }
              }
              if (disallowed) {
                  // ignore the disallowed header
                  continue;
              }
          }

          headers.add(std::move(name), std::move(value));
      }
  }

  HTTPResponse HTTPClientConnection::request(enum HTTTPRequestMethod method, bytes const & path,
                                             HTTPHeaders const & headers) {
      nlib::bytes message = getHTTTPRequestMethodBytes(method);
      message += " "_b;
      message += path;
      message += " HTTP/1.1\r\n"_b;
      message += headers.getBytes();
      message += "\r\n"_b;
      this->socket.write(message);

      if (this->socket.read(9) != "HTTP/1.1 "_b) {
          throw httpexception("bad http data");
      }

      bytes statusBytes = this->socket.read_until(" "_b);
      if (statusBytes.size() != 3) {
          throw httpexception("bad status code");
      }
      enum HTTPResponseStatus statusCode = toHTTPResponseStatus(statusBytes.parseU16f_UO());

      // discard the reason phrase (RFC 7230 Section 3.1.2)
      this->socket.read_until("\r\n"_b);

      HTTPHeaders responseHeaders {};
      this->parseHeaders(responseHeaders, false);

      bytes * body;
      if (responseHasBody(method, statusCode)) {
          bytes const * transferEncoding = responseHeaders.findFast("transfer-encoding"_b);

          if (transferEncoding == nullptr) {
              // no special encoding
              bytes const * contentLength = responseHeaders.findFast("content-length"_b);
              if (contentLength == nullptr) {
                  // content-length header is missing
                  // TODO
                  throw notimplemented("Content-Length missing");
              } else {
                  // ownership of this object will be passed to the object returned
                  body = new bytes(this->socket.read(contentLength->parseUInt()));
              }

          } else if (*transferEncoding == "chunked"_b) {
              body = new bytes();
              while (true) {
                  bytes chunkHeader = this->socket.read_until("\r\n"_b);
                  // TODO: support chunk-ext
                  if (chunkHeader == "0"_b) {
                      // chunked trailer
                      this->parseHeaders(responseHeaders, true);
                      break;
                  }
                  unsigned long long const chunkSize = chunkHeader.parseUIntHex();
                  *body += this->socket.read(chunkSize);
                  this->socket.read(2);  // discard CRLF
              }

          } else {
              // TODO
              throw notimplemented("Transfer-Encoding not implemented: " + transferEncoding->toString());
          }
      } else {
          body = nullptr;
      }

      // HTTPResponse takes ownership of the body object
      return HTTPResponse(std::move(responseHeaders), body, statusCode);
  }

  HTTPClientConnection HTTPClientConnection::connect(bytes const & hostname) {
      std::shared_ptr<nlib::Socket> sock = std::make_shared<nlib::tcp::TCPSocket>(nlib::tcp::TCPSocket::connect(
          nlib::dns::resolve(hostname),
          80
      ));
      return nlib::http::HTTPClientConnection(sock);
  }

}
