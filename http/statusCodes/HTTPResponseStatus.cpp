enum HTTPResponseStatus toHTTPResponseStatus(uint_fast16_t val) {
    if (!((val >= 100 && val <= 101) || (val >= 200 && val <= 206) || (val >= 300 && val <= 302) || (val >= 304 && val <= 304) || (val >= 307 && val <= 307) || (val >= 400 && val <= 417) || (val >= 500 && val <= 505))) {
        throw std::invalid_argument("Invalid response status");
    }
    return static_cast<enum HTTPResponseStatus>(val);
}
