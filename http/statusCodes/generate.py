#!/usr/bin/env python3
import json

with open('statusCode.json') as f:
    data = json.load(f)

status_codes_list = [number for _, values in data for number, _ in values]
if len(set(status_codes_list)) != len(status_codes_list):
    raise ValueError("Duplicate codes")
if status_codes_list != sorted(status_codes_list):
    raise ValueError("Codes must be in sorted order")

enum_decl = 'enum HTTPResponseStatus {\n'
for i, (comment, values) in enumerate(data):
    last_section = i == len(data) - 1
    enum_decl += f'    // {comment}\n'
    for j, (number, name) in enumerate(values):
        delimiter = '' if j == len(values) - 1 and last_section else ','
        cpp_name = f'HTTP_{number}_{name}'

        enum_decl += f'    {cpp_name} = {number}{delimiter}\n'

    if not last_section:
        enum_decl += '\n'
enum_decl += '};\n'

contiguous_sequences = []
in_contiguous_sequence = None
for number in status_codes_list:
    if in_contiguous_sequence is not None:
        start, last = in_contiguous_sequence
        if number == last + 1:
            in_contiguous_sequence = (start, number)
        else:
            contiguous_sequences.append((start, last))
            in_contiguous_sequence = None
    if in_contiguous_sequence is None:
        in_contiguous_sequence = (number, number)
if in_contiguous_sequence is not None:
    contiguous_sequences.append(in_contiguous_sequence)

condition = ' || '.join(f'(val >= {start} && val <= {end})' for start, end in contiguous_sequences)
func_def = ('enum HTTPResponseStatus toHTTPResponseStatus(uint_fast16_t val) {\n'
            '    if (!(' + condition + ')) {\n'
            '        throw std::invalid_argument("Invalid response status");\n'
            '    }\n'
            '    return static_cast<enum HTTPResponseStatus>(val);\n'
            '}\n')

func_decl = 'enum HTTPResponseStatus toHTTPResponseStatus(uint_fast16_t);\n'

with open('HTTPResponseStatus.h', 'w') as f:
    f.write(enum_decl)
    f.write(func_decl)

with open('HTTPResponseStatus.cpp', 'w') as f:
    f.write(func_def)
